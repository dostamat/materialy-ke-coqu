Inductive nularni_spojky :=
  | tt
  | ff.

Inductive unarni_spojky :=
  | negace.

Inductive binarni_spojky :=
  | konjunkce
  | disjunkce
  | implikace
  | ekvivalence.

Inductive vl_formule {At : Set} :=
  | atomicka (a : At)
  | nularni (n : nularni_spojky)
  | unarni (u : unarni_spojky) (f : vl_formule)
  | binarni (b : binarni_spojky) (f1 f2 : vl_formule).

Fixpoint formule_vyrok
  {At : Set} (atomy: At -> Prop) (f : @vl_formule At) : Prop :=
  match f with
  | atomicka a => atomy a
  | nularni n =>
    match n with
    | tt => True
    | ff => False
    end
  | unarni u f' =>
    match u with negace => ~ formule_vyrok atomy f' end
  | binarni b f1 f2 =>
    match b with
    | konjunkce => (formule_vyrok atomy f1) /\ (formule_vyrok atomy f2)
    | disjunkce => (formule_vyrok atomy f1) \/ (formule_vyrok atomy f2)
    | implikace => (formule_vyrok atomy f1) -> (formule_vyrok atomy f2)
    | ekvivalence => (formule_vyrok atomy f1) <-> (formule_vyrok atomy f2)
    end
  end.

Section test.

  Inductive moje_At :=
    | af_a
    | af_b
    | af_c.

  Definition formule1 :=
    binarni konjunkce (unarni negace (atomicka af_a)) (atomicka af_b).

  Hypotheses (A B C : Prop).

  Definition moje_atomy (atom : moje_At) : Prop :=
    match atom with
    | af_a => A
    | af_b => B
    | af_c => C
    end.

  Compute formule_vyrok moje_atomy
    (binarni konjunkce (unarni negace (atomicka af_a)) (atomicka af_b)).

End test.