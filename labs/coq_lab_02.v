Section negace_kontradikce.

  Hypotheses (P Q : Prop).

  (** Zde nutno znát pravidla pro negaci a False. *)

  (** Jak se chová negace?
  [~ P] je zkratka za [P -> False].
  Můžeme tedy používat taktiku [intro].*)

  (** Z [False] lze odvodit cokoli (to je pravidlo sporu "ex falso quodlibet").
  Můžeme využít taktiku [exfalso].*)
  Lemma not_contrad :  ~(P /\ ~P).
  Proof.
  Admitted.

  Lemma or_and_not : (P \/ Q) /\ ~P -> Q.
  Proof.
  Admitted.

  Lemma de_morgan_1 : ~(P \/ Q) -> ~P /\ ~Q.
  Proof.
  Admitted.

  Lemma de_morgan_2 : ~P /\ ~Q -> ~(P \/ Q).
  Proof.
  Admitted.

  Lemma de_morgan_3 : ~P \/ ~Q -> ~(P /\ Q).
  Proof.
  Admitted.

  Lemma not_not_exm : ~ ~ (P \/ ~ P).
  Proof.
  Admitted.

End negace_kontradikce.

Section min_logika_programy.

  (** Nyní budeme důkazy programovat! *)
  (** Je nutno předvést syntax pro aplikaci funkcí a pro abstrakci (anonymní funkce). *)
  Hypotheses P Q R S : Prop.

  Lemma test (p : P) (f : P -> Q) : Q.
  Proof.
  Admitted.

  Lemma test' (p : P) : (P -> Q) -> Q.
  Proof.
  Admitted.

  Lemma test'' : P -> (P -> Q) -> Q.
  Proof.
  Admitted.

  Print test''.

  Lemma test_tactics : P -> (P -> Q) -> Q.
  Proof.
    intros p f.
    apply f.
    apply p.
  Qed.

  Print test_tactics.

Lemma imp_trans : (P -> Q) -> ((Q -> R) -> (P -> R)).
  Proof
  Admitted.
  Lemma id_P : P -> P.
  Proof.
  Admitted.

  Lemma id_PP : (P -> P) -> P -> P.
  Proof.
  Admitted.

  Lemma ignore_Q : (P -> R) -> P -> Q -> R.
  Proof.
  Admitted.

  Lemma delta_impR : (P -> Q) -> P -> P -> Q.
  Proof.
  Admitted.

  Lemma imp_trans : (P -> Q) -> ((Q -> R) -> (P -> R)).
  Proof
  Admitted.

  Lemma delta_imp : (P -> P -> Q) -> P -> Q.
  Proof.
  Admitted.

  Lemma imp_dist : (P -> Q -> R) -> (P -> Q) -> P -> R.
  Proof.
  Admitted.

  Lemma imp_perm : (P -> Q -> R) -> Q -> P -> R.
  Proof.
  Admitted.

  Lemma diamond : (P -> Q) -> (P -> R) -> (Q -> R -> S) -> P -> S.
  Proof.
  Admitted.

  Lemma weak_peirce : ((((P -> Q) -> P) -> P) -> Q) -> Q.
  Proof.
  Admitted.

End min_logika_programy.