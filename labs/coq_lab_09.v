Require Import Arith ZArith.

(** [sum_f : nat -> (nat -> Z) -> Z]
pro hodnotu [n] a funkci [f : nat -> Z]
secte vsechny hodnoty [(f m)]
pro [m < n].*)
Fixpoint sum_f (n : nat) (f : nat -> Z) : Z :=
  match n with
  | 0 => 0
  | S n' => f n' + sum_f n' f
  end.

(** Definujme induktivni datovy typ binarnich stromu
s celymi cisly v nelistovych vrcholech: *)
Inductive Z_btree : Set.

(** Jaky je typ induktivniho principu pro binarni stromy? *)
Check Z_btree_ind.

(** Definujte funkce scitajici hodnoty ve stromu
a zjistujici pritomnost nuly mezi hodnotami ve stromu. *)

Fixpoint sum_all_values (t : Z_btree) : Z.
Admitted.

Fixpoint zero_present (t : Z_btree) : Z.
Admitted.

(** Definujte induktivni typ representujici jazyk
vyrokove logiky bez promennych, jen s konstantami
pravda a nepravda, a s logickymi spojkami negace,
konjunkce, disjunkce a implikace. *)

(** ----------------------------- *)

(** Alternativni zpusob definice binarnich stromu: *)
Inductive Z_fbtree : Set.

(** Definujte funkci [fleft_son] ([fright_son])
vracejici levou (ci pravou) vetev
binarniho stromu [t: Z_fbtree].
(U listu vratte list.) *)

(** Prostudujte induktivni princip pro [Z_fbree].
Popiste vlastnimi slovy, co rika. *)
Check Z_fbtree_ind.

(** Definujte funkce scitajici hodnoty ve stromu
a zjistujici pritomnost nuly mezi hodnotami ve stromu
typu [Z_fbtree].
([fsum_all_values] a [fzero_present])*)

(*--------------------*)

(** Stromy s nekonecnym vetvenim *)
Inductive Z_inf_branch_tree : Set.

(** Definujte funkci scitajici vsechny hodnoty
ve stromu typu [Z_inf_branch_tree], ktere jsou
ulozeny v indexech mensich nez [n]. *)
(** Vyuzijte vami naprogramovanou funkci [sum_f]. *)

(** Definujte funkci rozhodujici, zda strom typu
[Z_inf_branch_tree] obsahuje hodnotu [0] v nekterem
z vrcholu s indexem mensim nez [n].*)

(* ------------------------------- *)

(** Uz jsme videli polymorfni typy,
induktivni definice mohou ale byt
parametrizovany jinymi nez typovymi hodnotami. *)
(** Definice stromu, ktery v sobe pripousti
hodnoty ohranicene cislem [n]: *)
Inductive ltree (n : nat) : Set :=
  | lleaf : ltree n
  | lnode : forall (p : nat),
            p <= n ->
            ltree n -> ltree n -> ltree n.

(** Jelikoz je [n] v predchozi definici
parametrem, musi byt [n] pouzito u vsech
vyskytu [ltree] v definici. *)

(** Coq umoznuje i volnejsi definice:
binarni strom s hodnotami ve vsech vrcholech,
+ *podminka*: obe vetve stromu musi mit stejnou vysku. *)
Inductive htree (A : Set) : nat -> Set :=
  | hleaf : A -> htree A 0
  | hnode : forall (n : nat),
            A ->
            htree A n ->
            htree A n ->
            htree A (S n).

(** Zkuste pochopit, co rika indukcni princip
pro typ [htree]: *)
Check htree_ind.

(** Definujte funkci [htree_to_btree]. *)
(** Definujte funkci [invert], ktera
invertuje strom [t: htree A n].
Coq za vas dokonce zvladne doplnit
dependent pattern matching konstrukt. *)
Fixpoint invert (A : Set) (n : nat) (t : htree A n) : htree A n.
Admitted.

Print invert.

(** Induktivne definovat lze nejen
"mnoziny", ale i predikaty: *)

(** Induktivni definice sudych prirozenych cisel: *)
Inductive ev : nat -> Prop :=
  | ev_0 : ev 0
  | ev_SS : forall (n : nat), ev n -> ev (S (S n)).

(** Induktivni definice relace "mensi nebo rovno": *)
Inductive le (n : nat) : nat -> Prop :=
  | le_n : le n n
  | le_S : forall (m : nat), le n m -> le n (S m).

(** Induktivni definice usporadaneho listu: *)
Inductive sorted (A : Set) (R : A -> A -> Prop) :
  list A -> Prop :=
  | sorted0 : sorted A R nil
  | sorted1 : forall (a : A), sorted A R (cons a nil)
  | sorted2 : forall (a b : A) (l : list A),
              R a b ->
              sorted A R (cons b l) ->
              sorted A R (cons a (cons b l)).

Arguments sorted {A}.

(**Dokazte, ze 4 je suda: *)
Lemma ev_4 : ev 4.
Proof.
Admitted.

Fixpoint double (n : nat) : nat :=
  match n with
  | O => O
  | S n' => S (S (double n'))
  end.

(**Dokazte, ze dvojnasobek kazdeho cisla
je cislo sude.*)
Lemma ev_double : forall (n : nat), ev (double n).
Proof.
Admitted.

(** Dokazte "inverzni" lemma: *)
Lemma ev_inversion :
  forall (n : nat), ev n ->
  (n = 0) \/ (exists n', n = S (S n') /\ ev n').
Proof.
Admitted.

(* Dokazte (staci [destruct]):*)
Lemma ev_minus2 :
  forall (n : nat), ev n -> ev (pred (pred n)).
Proof.
Admitted.

(* Dokazte, na predpoklad uzijte [ev_inversion]
([apply ev_inversion in H]).
*)
Lemma evSS_ev :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
Admitted.

(** Jednodussi zpusob je pouzit taktiku [inversion].
  Ta automatizuje praci, kterou jsme delali manualne. *)
Lemma evSS_ev' :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
Admitted.

(** Dokazte, ze 1 neni suda.
([inversion] pomaha.) *)
Lemma one_not_ev : ~ ev 1.
Proof.
Admitted.


(** Dokazte s vyuzitim [inversion] :*)
Theorem SSSSev__even : forall n,
  ev (S (S (S (S n)))) -> ev n.
Proof.
Admitted.

(** Dokazte s vyuzitim [inversion] :*)
Theorem ev5_nonsense :
  ev 5 -> 2 + 2 = 9.
Proof.
Admitted.

(** Alternativni, "neinduktivni" definice sudosti: *)
Definition Even x := exists n : nat, x = double n.

(** Obcas je treba pouzit indukci podle predikatu: *)
(** (nezapomenme na [unfold]!) *)
Lemma ev_Even : forall n,
  ev n -> Even n.
Proof.
  intros n H.
  induction H.
  - unfold Even.
    exists 0.
    reflexivity.
  - unfold Even in *.
    destruct IHev.
    rewrite H0.
    exists (S x).
    reflexivity.
Qed.

(** Dokazte indukci podle predikatu: *)
Lemma ev_sum : forall (n m : nat), ev n -> ev m -> ev (n + m).
Proof.
Admitted.

(** Dokazte obraceny smer ekvivalence nasich dvou
definici sudosti. *)
Lemma Even_ev : forall n, Even n -> ev n.
Proof.
Admitted.

(** Zde je dalsi mozna definice sudosti: *)
Inductive ev' : nat -> Prop :=
  | ev'_0 : ev' 0
  | ev'_2 : ev' 2
  | ev'_sum : forall (n m : nat),
              ev' n -> ev' m -> ev' (n + m).

(** Dokazte ekvivalenci techto dvou
induktivnich definic. *)
Lemma ev'_ev : forall (n : nat), ev' n <-> ev n.
Proof.
  split.
Admitted.

(** Dokazte indukci (ale podle ceho?): *)
Theorem ev_ev__ev : forall n m,
  ev (n+m) -> ev n -> ev m.
Proof.
Admitted.