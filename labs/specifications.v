Check sig.
Print sig.

Check @sig nat (fun n => n < 5).
Check sig (fun n => n < 5).

Check {n : nat | n < 5}.

Locate "<".
Print lt.

Lemma le_3_5 : 3 < 5.
Proof.
  repeat constructor.
Qed.

(** Trojka jako prvek mnoziny {n | n < 5}*)
Definition three_in_subset := exist (fun n => n < 5) 3 le_3_5.

Definition extract (A : Set) (P : A -> Prop) : sig P -> A :=
  fun (x : sig P) =>
  match x with
  | exist _ a Pa => a
  end.

Compute extract _ _ three_in_subset.

Set Printing All.


Check sumbool.
Print sumbool.

Definition eq_dec (A : Set) := forall x y : A, {x = y} + {~ x = y}.

Lemma nat_eq_dec : eq_dec nat.
Proof.
  unfold eq_dec.
  intros x.
  induction x.
  - intros y.
    destruct y.
    + left. reflexivity.
    + right. discriminate.
  - intros y.
    destruct y.
    + right. discriminate.
    + pose proof (IHx y) as IHxy.
      destruct IHxy.
      -- left. rewrite e. reflexivity.
      -- right. intro. injection H as K. apply n, K.
Qed.

Require Import Arith.
Check Nat.eq_dec.

Check sumor.
Print sumor.

Check forall n : nat, (sumor { p : nat | n = S p} (n = 0)).

Check forall n : nat, {p : nat | n = S p} + {n = 0}.

Locate "_ = _".
Print eq.

Definition spec_pred (n : nat) : {p : nat | n = S p} + {n = 0} :=
  match n with
  | O => inright _ (eq_refl 0)
  | S q => inleft _ (exist (fun p' : nat => S q = S p') q (eq_refl (S q)))
  end.

Definition spec_pred' (n : nat) : {p : nat | n = S p} + {n = 0}.
destruct n.
- right. reflexivity.
- left. apply (exist _ n). reflexivity.
Defined.

Require Extraction.

Extraction Language OCaml.
Extraction spec_pred'.

Print spec_pred'.

Compute spec_pred 5.
Compute spec_pred 0.

Inductive natree : Set :=
  | leaf (n : nat) : natree
  | node (n : nat) (t1 t2 : natree) : natree.

Check node 5 (node 3 (leaf 2) (leaf 5)) (leaf 1).

Inductive occurs (n : nat) : natree -> Prop :=
  | inleaf : occurs n (leaf n)
  | inroot (t1 t2 : natree) : occurs n (node n t1 t2)
  | inleft
      (m : nat)
      (t1 t2 : natree)
      (H: occurs n t1) : occurs n (node m t1 t2)
  | inright
      (m : nat)
      (t1 t2 : natree)
      (H: occurs n t2) : occurs n (node m t1 t2).

Definition naive_occurs_dec (n : nat) (t : natree) :
  {occurs n t} + {~ occurs n t}.
  induction t.
  - Search ({ _ = _ } + { ~ _ = _ }).
    pose (Nat.eq_dec n n0) as K.
    destruct K as [K1 | K2].
    + left; rewrite K1; constructor.
    + right; intro. apply K2.
      inversion H. reflexivity.
  - destruct IHt1, IHt2; try (left; constructor; assumption).
    pose (Nat.eq_dec n n0) as K.
    destruct K as [K1 | K2].
    + left; rewrite K1; constructor.
    + right; intro; inversion H; tauto.
Defined.

Inductive lower (n : nat) (t : natree) : Prop :=
  | lower_intro
      (H : forall m, (occurs m t) -> n < m) : lower n t.

  Inductive upper (n : nat) (t : natree) : Prop :=
  | upper_intro
      (H : forall m, (occurs m t) -> m < n) : upper n t.

Inductive search_tree : natree -> Prop :=
  | leaf_search (n : nat) : search_tree (leaf n)
  | node_search
      (n : nat)
      (t1 t2 : natree)
      (ST1 : search_tree t1)
      (ST2 : search_tree t2)
      (U : upper n t1)
      (L : lower n t2) : search_tree (node n t1 t2).

Definition occurs_dec_spec (n : nat) (t : natree) : Set :=
  search_tree t -> {occurs n t} + {~occurs n t}.

(* Definition occurs_dec :
  forall (n : nat) (t : natree), occurs_dec_spec n t.
Admitted. *)

Inductive INSERT (n : nat) (t t' : natree) : Prop :=
  | insert_intro
    (S : forall (m : nat), occurs m t -> occurs m t')
    (O : occurs n t')
    (S' : forall (m : nat), occurs m t' ->
                              occurs m t \/ n = m)
    (ST : search_tree t') : INSERT n t t'.

Definition insert_spec (n : nat) (t : natree) : Set :=
  search_tree t -> { t' : natree | INSERT n t t' }.

Definition insert :
  forall (n : nat) (t : natree), insert_spec n t.
Admitted.

Inductive REMOVE (n : nat) (t t' : natree) : Prop :=
  | remove_intro
    (H : ~ occurs n t')
    (S : forall (m : nat), occurs m t' -> occurs m t)
    (S' : forall (m : nat), occurs m t ->
                              occurs m t' \/ n = m)
    (ST : search_tree t') : REMOVE n t t'.

Definition remove_spec (n : nat) (t : natree) : Set :=
  search_tree t -> { t' : natree | REMOVE n t t'}.

Definition remove (n : nat) (t : natree) : remove_spec n t.
Admitted.

Search ({ _ <= _ } + { ~ _ <= _} ).
Print le_lt_dec.

Lemma lower_leaf (n m : nat) (H : n < m) : lower n (leaf m).
Proof.
  constructor.
  intro m'.
  intro K.
  inversion K.
  assumption.
Qed.

Lemma upper_leaf (n m : nat) (H : m < n) : upper n (leaf m).
Proof.
  constructor.
  intro m'.
  intro K.
  inversion K.
  assumption.
Qed.

Lemma lower_not_occurs (n : nat) (t : natree) :
  lower n t -> ~ occurs n t.
Proof.
  intro H.
  intro O.
  destruct H.
  apply H in O.
  apply (Nat.lt_irrefl n); assumption.
Qed.

Lemma upper_not_occurs (n : nat) (t : natree) :
  upper n t -> ~ occurs n t.
Proof.
  intro H.
  intro O.
  destruct H.
  apply H in O.
  apply (Nat.lt_irrefl n); assumption.
Qed.

Section properties.

Context
  (n : nat)
  (t1 t2 : natree)
  (se : search_tree (node n t1 t2)).

Lemma search_tree_left : search_tree t1.
Proof.
  inversion se; assumption.
Qed.

Lemma search_tree_right : search_tree t2.
Proof.
  inversion se; assumption.
Qed.

Lemma upper_left : upper n t1.
Proof.
  inversion se; assumption.
Qed.

Lemma lower_right : lower n t2.
Proof.
  inversion se; assumption.
Qed.

Lemma not_right (m : nat) : m <= n -> ~ occurs m t2.
Proof.
  intro; intro; inversion se; inversion L.
  specialize (H1 m H0).
  unfold lt in H1.
  Search ( ( _ <= _ ) -> ( _ <= _ ) -> ( _ <= _ ) ).
  pose proof (Nat.le_trans (S n) m n) H1 H.
  Search  ( ~ S _ <= _ ).
  apply ((Nat.nle_succ_diag_l n) H5).
Qed.

Lemma not_left (m : nat) : m >= n -> ~ occurs m t1.
Proof.
  intro; intro. inversion se. inversion U.
  specialize (H1 m H0).
  unfold lt in H1.
  unfold ge in H.
  pose proof (Nat.le_trans (S m) n m) H1 H.
  apply ((Nat.nle_succ_diag_l m) H5).
Qed.

Lemma go_left (m : nat) :
  occurs m (node n t1 t2) -> m < n -> occurs m t1.
Proof.
  intros.
  inversion H.
  - unfold lt in H0.
    rewrite H2 in H0.
    exfalso.
    apply ((Nat.nle_succ_diag_l n) H0).
  - assumption.
  - exfalso.
    pose proof lower_right.
    destruct H5; specialize (H5 m H2).
    unfold lt in H0, H5.
    Search ( ( S _ ) <= _ -> _ <= _ ).
    apply le_Sn_le in H0.
    pose proof (Nat.le_trans (S n) m n) H5 H0.
    apply ((Nat.nle_succ_diag_l n) H6).
Qed.

Lemma go_right (m : nat) :
  occurs m (node n t1 t2) -> m > n -> occurs m t2.
Proof.
  intros.
  inversion H.
  - unfold lt in H0.
    rewrite H2 in H0.
    exfalso.
    apply ((Nat.nle_succ_diag_l n) H0).
  - exfalso.
    pose proof upper_left.
    destruct H5; specialize (H5 m H2).
    unfold gt, lt in H0, H5.
    Search ( ( S _ ) <= _ -> _ <= _ ).
    apply le_Sn_le in H5.
    pose proof (Nat.le_trans (S n) m n) H0 H5.
    apply ((Nat.nle_succ_diag_l n) H6).
  - assumption.
Qed.

End properties.

#[ export ] Hint Resolve
  go_left
  go_right
  not_left
  not_right
  search_tree_left
  search_tree_right
  lower_right
  upper_left : searchtrees.

#[ export ] Hint Constructors occurs : searchtrees.

Definition occurs_dec :
  forall (n : nat) (t : natree), occurs_dec_spec n t.
Proof.
  intros m t.
  unfold occurs_dec_spec.
  intro ST.
  induction t.
  - destruct (Nat.eq_dec n m) as [H | H].
    + left; rewrite H; constructor.
    + right; intro K; inversion K. congruence.
  - elim (IHt1 (search_tree_left n t1 t2 ST)).
    (* why elim? destruct would introduce
    a let construct in the extracted program. *)
    + left. auto with searchtrees.
    + elim (le_gt_dec m n); intros.
      elim (le_lt_eq_dec _ _ a); intros.
      * right. eauto with searchtrees.
      * left. subst. eauto with searchtrees.
      * specialize (IHt2 (search_tree_right _ _ _ ST)).
        destruct IHt2.
        -- left. auto with searchtrees.
        -- eauto with searchtrees.
Defined.

Extraction natree.
Extract Inductive sumbool => "bool" ["true" "false"].
Extraction occurs_dec.

Lemma insert_leaf_l (m n : nat) (H : m < n) :
  INSERT m (leaf n) (node ).

(** Other implementations *)

(*
Definition occurs_dec :
  forall (n : nat) (t : natree), occurs_dec_spec n t.
Proof.
  intros m t.
  unfold occurs_dec_spec.
  intro ST.
  induction t.
  - destruct (Nat.eq_dec n m) as [H | H].
    + left; rewrite H; constructor.
    + right; intro K; inversion K. congruence.
  - specialize (IHt1 (search_tree_left n t1 t2 ST)).
    destruct (le_gt_dec m n) as [H | H].
    apply le_lt_eq_dec in H.
    destruct H as [H | H].
    + destruct IHt1.
      * left. auto with searchtrees.
      * right. intro K. inversion K.
      -- rewrite H1 in H.
      Search ( ~ _ < _).
      apply (Nat.lt_irrefl n); assumption.
      -- auto.
      -- eauto with searchtrees.
    + left. rewrite H. auto with searchtrees.
    + specialize (IHt2 (search_tree_right _ _ _ ST)).
      destruct IHt2.
      * left. auto with searchtrees.
      * right. eauto with searchtrees.
Qed.
*)

(*
Definition occurs_dec :
  forall (n : nat) (t : natree), occurs_dec_spec n t.
Proof.
  intros m t.
  unfold occurs_dec_spec.
  intro ST.
  induction t.
  - pose proof (Nat.eq_dec n m) as [H | H].
    + left; rewrite H; constructor.
    + right; intro K; inversion K. congruence.
  - pose proof (search_tree_left _ _ _ ST) as ST1.
    pose proof (search_tree_right _ _ _ ST) as ST2.
    apply IHt1 in ST1.
    apply IHt2 in ST2.
    clear IHt1 IHt2.
    destruct ST1, ST2.
    + left. apply inleft; assumption.
    + left. apply inleft; assumption.
    + left. apply inright; assumption.
    + pose proof (Nat.eq_dec m n).
      destruct H.
      * left. rewrite e. apply inroot.
      * right. intro. inversion H; auto.
Qed.
*)
