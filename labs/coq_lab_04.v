Print nat.

Print Nat.add.
Compute (Nat.add 2 4).
(** Misto [Nat.add] lze psat i [plus] (zkracena notace). *)
Compute (plus 2 4).

(** Lze take pouzivat infixni notaci [_ + _]. *)
Compute 2 + 4.

(** Pomoci [Locate] lze hledat definice notaci. *)
Locate "_ + _".


Print Nat.mul.
Compute (Nat.mul 2 4).
(** misto [Nat.mul] lze psat i [mult] *)
Compute (mult 2 4).

(** Lze take pouzivat infixni notaci [_ * _]. *)
Compute 2 * 4.

(** Definice dostupne z modulu [Nat]: *)
Print Nat.

(** Dokazte jednoducha tvrzeni plynouci
primo z definic operaci [+] a [*]: *)
Lemma plus_O_n : forall n : nat, 0 + n = n.
Proof.
Admitted.

Lemma mult_0_l : forall n : nat, 0 * n = 0.
Proof.
Admitted.

(** Dokazte pomocna tvrzeni o scitani
a o nasobeni indukci (taktika [induction]): *)
Lemma plus_n_O : forall n : nat, n + 0 = n.
Proof.
Admitted.

Lemma mult_n_O : forall n : nat, n * 0 = 0.
Proof.
Admitted.

Lemma plus_n_Sm : forall n m : nat, S (n + m) = n + S m.
Proof.
Admitted.

Lemma plus_Sn_m : forall n m : nat, S n + m = S (n + m).
Proof.
Admitted.

(** Formulujte a dokazte zakladni vlastnosti
operaci scitani a nasobeni: komutativitu, asociativitu, distributivitu. *)
(** Muzete pouzivat jiz dokazana tvrzeni pomoci [rewrite]
(a v druhem smeru [rewrite <-]). *)
(** Muzete si pomoci dokazovanim pomocnych tvrzeni (lemmat). *)
(** Obcas nepotrebujete vyuzit indukcni predpoklad.
V takovych pripadech lze nahradit taktiku [induction]
taktikou [destruct], ktera indukcni predpoklad nezavede. *)

Theorem plus_commutativity : forall n m : nat, n + m = m + n.
Proof.
  intro n.
  induction n as [ | n' IHn'].
  (* ^ V zakladnim kroku nezavadime nove identifikatory.
  V indukcnim kroku oznacujeme predchudce [n] jako [n']
  a indukcni predpoklad jako [IHn']
  (jmena si muzete sami zvolit). *)
  - intro m.
    rewrite plus_n_O.
    simpl.
    reflexivity.
  - admit.
Admitted.

(* Theorem plus_associativity : ... *)



(** Hezke cviceni prebrane z Logical Foundations: *)

(** Pripomenuti funkce "mensi nebo rovno?"
a "rovno?" *)
Fixpoint leb (n m : nat) : bool :=
  match n with
  | O => true
  | S n' =>
      match m with
      | O => false
      | S m' => leb n' m'
      end
  end.

Fixpoint eqb (n m : nat) : bool :=
  match n with
  | O => match m with
         | O => true
         | S m' => false
         end
  | S n' => match m with
            | O => false
            | S m' => eqb n' m'
            end
  end.

(** Infixni notace pro [leb] a [eqb]. *)
Notation "x <=? y" := (leb x y) (at level 70) : nat_scope.
Notation "x =? y" := (eqb x y) (at level 70) : nat_scope.

(**
U kazdeho z nasledujich tvrzeni si *nejprve* promyslete, zda
1. pujde dokazat jen pomoci [simpl] a [rewrite],
2. bude nutny rozbor pripadu [destruct],
3. bude nutna indukce [induction].

*Potom* zkuste dane tvrzeni dokazat. *)

Theorem leb_refl : forall n : nat,
  (n <=? n) = true.
Proof.
Admitted.

Theorem zero_neqb_S : forall n:nat,
  0 =? (S n) = false.
Proof.
Admitted.

Theorem andb_false_r : forall b : bool,
  andb b false = false.
Proof.
Admitted.

Theorem plus_leb_compat_l : forall n m p : nat,
  n <=? m = true -> (p + n) <=? (p + m) = true.
Proof.
Admitted.

Theorem S_neqb_0 : forall n:nat,
  (S n) =? 0 = false.
Proof.
Admitted.

Theorem mult_1_l : forall n:nat, 1 * n = n.
Proof.
Admitted.

Theorem all3_spec : forall b c : bool,
  orb
    (andb b c)
    (orb (negb b)
         (negb c))
  = true.
Proof.
Admitted.
