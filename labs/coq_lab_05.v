Section unarni_predikaty.

  Context (U : Set) (P Q R : U -> Prop).
  
  (** Priklady s obecnymi kvantifikatory. *)

  (** K praci s obecnym kvantifikatorem
pouzivame taktiky
[intro] (popripade [intros]) a [apply]. *)

  Lemma antecedent_out (a : U) :
    (forall x, P a -> Q x) -> (P a -> forall z, Q z).
  Proof.
    intro H.
    intro pa.
    intro z.
    apply H.
    apply pa.
  Qed.
  
  Lemma antecedent_out' (a : U) :
    (forall x, P a -> Q x) -> (P a -> forall z, Q z).
  Proof.
    intros H pa z.
    pose proof (H z) as K.
    pose proof (K pa) as L.
    assumption.
  Qed.

  Lemma factor_forall_out :
    (forall x, P x) /\ (forall y, Q y) ->
    (forall z, P z /\ Q z).
  Proof.
    intros [allP allQ].
    intro z.
    split.
    - apply allP.
    - apply allQ.
  Qed.

  Lemma allPQ_allP_allQ : 
    (forall x : U, P x -> Q x) ->
    (forall y, P y) ->
    forall z, Q z.
  Proof.
    intros H K z.
    apply H, K.
  Qed.

  Lemma P_or_Q_but_not_P
   (H : forall x, P x \/ Q x)
   (K : forall y, ~ P y) :
   forall x, Q x.
  Proof.
    intro x.
    pose proof (H x) as Hx.
    pose proof (K x) as notPx.
    destruct Hx as [Px | Qx].
    - exfalso.
      unfold not in *.
      apply notPx.
      apply Px.
    - assumption.
  Qed.

  Lemma PQ_QR_PR
    (H : forall x, P x -> Q x)
    (K : forall x, Q x -> R x) :
    forall x, P x -> R x.
  Proof.
    intros x Px.
    apply ((K x) ((H x) Px)).
  Qed.

  (** Priklady s existencnimi kvantifikatory. *)

  (** K praci s existencnim kvantifikatorem
pouzivame taktiky [exists] a [destruct]. *)

  Lemma exists_consequent
    (a : U) (H : exists x, P a -> Q x) :
    P a -> exists y, Q y.
  Proof.
    intro Pa.
    destruct H as [b K].
    exists b.
    apply K.
    apply Pa.
  Qed.
  
  Lemma factor_exists_in_conj :
    (exists x, P x /\ Q x) ->
    (exists y, P y) /\ (exists z, Q z).
  Proof.
    intros [a [Pa Qa]]; split; exists a; assumption.
  Qed.
  
  Lemma factor_exists_in_disj :
    (exists x, P x \/ Q x) ->
    (exists y, P y) \/ (exists z, Q z).
  Proof.
    intros [a [Pa | Qa]];
    [left | right];
    exists a;
    assumption.
  Qed.
  
End unarni_predikaty.

Section binarni_predikaty.

  Context (U : Set) (R : U -> U -> Prop).
  
  Lemma all_diagonal :
    (forall x, forall y, R x y) -> forall z, R z z.
  Proof.
    intro H.
    intro z.
    apply H.
  Qed.
  
  Lemma technical (H : forall x y, R x y) :
    forall z, (R z z /\ forall t, R t z).
  Proof.
    split.
    - apply H.
    - intro t.
      apply H.
  Qed.

  Lemma ex_x_y_Rxy_Ryx (u : U) :
    exists x y, R x y -> R y x.
  Proof.
    exists u.
    exists u.
    intro H.
    assumption.
  Qed.

End binarni_predikaty.


Section smisene_ulohy.

  Context (U : Set) (P Q S : U -> Prop).

  Lemma mix1
    (H : forall x, P x -> Q x)
    (K : exists y, P y) :
    exists z, Q z.
  Proof.
    destruct K as [a Pa].
    exists a.
    apply H, Pa.
  Qed.

  Lemma mix2
    (H : forall x, Q x -> S x)
    (K : exists y, P y /\ Q y) :
    exists z, P z /\ S z.
  Proof.
    destruct K as [a [Py Qy]].
    exists a.
    split; try apply H; assumption.
  Qed.

  Lemma mix3
  (H : exists x, P x)
  (K : forall y, forall z, P y -> Q z) :
    forall t, Q t.
  Proof.
    intro t.
    destruct H as [a Pa].
    pose proof (K a) as H.
    pose proof (H t) as L.
    apply L, Pa.
  Qed.
  
End smisene_ulohy.


Section slovni_uloha.

  (*
Every young and healthy person likes baseball.

Every active person is healthy.

Someone is young and active.

Therefore, someone likes baseball.
*)
  Context (U : Set) (A B Y H : U -> Prop).

  Lemma someone_likes_baseball
    (K : forall x, (Y x /\ H x) -> B x)
    (L : forall y, A y -> H y)
    (M : exists z, Y z /\ A z) :
    exists t, B t.
  Proof.
    destruct M as [a [Ya Aa]].
    exists a.
    auto.
  Qed.

End slovni_uloha.


Section slovni_uloha2.

(**
'1: Všichni psi vyjí.
'2: Kdokoli, kdo má kocku, nemá žádnou myš.
'3: Kdo má problémy se spaním, nemá nic, co vyje.
'4: Jan má kocku nebo psa.
'5: Pokud má Jan problémy se spaním, nemá žádné myši.
*)

  Context (U : Set)
          (Pes Kocka Mys Vyje PSS : U -> Prop)
          (Ma : U -> U -> Prop).

End slovni_uloha2.

(**
'1 Každý clovek má právo sníst cokoli hloupejšího, než je jakýkoli vepr.
'2 Pašík je vepr chytrejší než jakékoli nemluvne.
'3 Každý clovek má právo sníst jakékoli nemluvne.
*)