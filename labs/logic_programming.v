Section test.

Print plus.

Print Nat.add.

Inductive plusR : nat -> nat -> nat -> Prop :=
  | PlusO : forall m : nat, plusR O m m
  | PlusS : forall n m r : nat, plusR n m r
    -> plusR (S n) m (S r).

Hint Constructors plusR : lp.

Theorem plus_plusR : forall n m : nat, plusR n m (n + m).
Proof.
  intros; induction n; simpl; auto with lp.
Qed.

Theorem plusR_plus : forall n m r : nat,
  plusR n m r -> r = n + m.
Proof.
  intros;
  induction H;
  try reflexivity;
  subst;
  reflexivity.
Qed.

Example four_plus_three : 4 + 3 = 7.
Proof. reflexivity. Qed.

Print four_plus_three.

Example four_plus_three' : plusR 4 3 7.
Proof.
  auto with lp.
Qed.

Print four_plus_three'.

Example five_plus_three : plusR 5 3 8.
Proof.
  info_auto 6 with lp.
Qed.

Print five_plus_three.

Example seven_minus_three :
  exists x, x + 3 = 7.
Proof.
  exists 4;
  reflexivity.
Qed.

Example seven_minus_three' :
  exists x, plusR x 3 7.
Proof.
  eapply ex_intro.
  apply PlusS.
  apply PlusS.
  apply PlusS.
  apply PlusS.
  apply PlusO.
Restart.
  info_eauto 6 with lp.
Qed.

Example seven_minus_four' :
  exists x, plusR 4 x 7.
Proof.
  eauto 6 with lp.
Qed.

SearchRewrite (O + _).

Hint Immediate plus_O_n : lp.

Lemma plusS : forall n m r,
  n + m = r
  -> S n + m = S r.
Proof.
  intros; simpl; subst; reflexivity.
Qed.

Hint Resolve plusS : lp.

Example seven_minus_three'' :
  exists x, x + 3 = 7.
Proof.
  info_eauto 6 with lp.
Qed.

Example seven_minus_four :
  exists x, 4 + x = 7.
Proof.
  info_eauto 5 with lp.
Qed.

Example seven_minus_four_zero :
  exists x, 4 + x + 0 = 7.
Proof.
  eauto 8 with lp.
Abort.

Lemma plusO : forall n m : nat,
  n = m
  -> n + 0 = m.
Proof.
  intros; subst; induction m; try reflexivity.
  - SearchRewrite (S _ + _).
    rewrite plus_Sn_m; rewrite IHm; reflexivity.
Qed.

Hint Resolve plusO : lp.

Example seven_minus_four_zero :
exists x, 4 + x + 0 = 7.
Proof.
  eauto 6 with lp.
Qed.

Check eq_trans.

Section slow.

Hint Resolve eq_trans : lp.

Example zero_minus_one :
  exists x, 1 + x = 0.
Proof.
  Time eauto 1 with lp.
  Time eauto 2 with lp.
  (*Time eauto 3 with lp.
  Time eauto 4 with lp.
  Time eauto 5 with lp.
  Time eauto 6 with lp.
  debug eauto 3 with lp.*)
Abort.

End slow.

(* Searching for underconstrained values *)

Print length.

Example length_1_2 : length (1 :: 2 :: nil) = 2.
Proof.
  reflexivity.
Qed.

Print length_1_2.

Theorem length_O :
  forall A, length (nil (A := A)) = O.
Proof.
  reflexivity.
Qed.

Print length_O.

Theorem length_S :
  forall A (h : A) t n,
  length t = n
  -> length (h :: t) = S n.
Proof.
  intros; simpl; subst; reflexivity.
Qed.

Hint Resolve length_O length_S : lp.

(* Example length_is_2 : exists ls : list nat, length ls = 2.
Proof.
  eauto with lp.
  Show Proof.
Qed. *)

Require Import List.

Print Forall.

Example length_is_2 : exists ls : list nat,
  length ls = 2 /\ Forall (fun n => n >= 1) ls.
Proof.
  eauto 9 with lp.
Qed.

Print length_is_2.

Print fold_right.

Definition sum := fold_right plus O.

Lemma plusO' :
  forall n m, n = m -> 0 + n = m.
Proof.
  auto.
Qed.

Hint Resolve plusO' : lp.

Hint Extern 1 (sum _ = _) => simpl : lp.

Example length_and_sum :
  exists ls : list nat,
  length ls = 2 /\ sum ls = O.
Proof.
  eauto 7 with lp.
Qed.

Example length_and_sum' :
  exists ls : list nat,
  length ls = 5 /\ sum ls = 42.
Proof.
  eauto 15 with lp.
Qed.

Example length_and_sum'' :
  exists ls : list nat,
  length ls = 3 /\ sum ls = 3
  /\ Forall (fun n => n <> 0) ls.
Proof.
  eauto 15 with lp.
Qed.

Print length_and_sum''.

(* Synthesizing programs *)

Inductive exp : Set :=
  | Const : nat -> exp
  | Var : exp
  | Plus : exp -> exp -> exp.

Inductive eval (var : nat) : exp -> nat -> Prop :=
  | EvalConst : forall n, eval var (Const n) n
  | EvalVar : eval var Var var
  | EvalPlus : forall e1 e2 n1 n2,
    eval var e1 n1
    -> eval var e2 n2
    -> eval var (Plus e1 e2) (n1 + n2).

Hint Constructors eval : lp.

Example eval1 :
  forall var,
  eval var (Plus Var (Plus (Const 8) Var))
           (var + (8 + var)).
Proof.
  auto with lp.
Qed.

Example eval1' :
  forall var, eval var (Plus Var (Plus (Const 8) Var)) (2 * var + 8).
Proof.
  eauto with lp.
Abort.

Theorem EvalPlus' : forall var e1 e2 n1 n2 n,
    eval var e1 n1
    -> eval var e2 n2
    -> n1 + n2 = n
    -> eval var (Plus e1 e2) n.
Proof.
  intros. rewrite <- H1. constructor; assumption.
Qed.

Hint Resolve EvalPlus' : lp.

Require Import Lia.

Hint Extern 1 (_ = _) => abstract lia : lp.

Example eval1' :
  forall var, eval var (Plus Var (Plus (Const 8) Var)) (2 * var + 8).
Proof.
  eauto with lp.
Qed.

Print eval1'.

Example synthesize1 : exists e, forall var, eval var e (var + 7).
Proof.
  eauto with lp.
Qed.

Print synthesize1.

Example synthesize2 : exists e, forall var, eval var e (2*var + 8).
Proof.
  eauto with lp.
Qed.

Print synthesize2.

Example synthesize3 : exists e, forall var, eval var e (3*var + 42).
Proof.
  eauto with lp.
Qed.

Print synthesize3.