Section negace_kontradikce.

  Hypotheses (P Q : Prop).

  (** Zde nutno znát pravidla pro negaci a False. *)

  (** Jak se chová negace?
  [~ P] je zkratka za [P -> False].
  Můžeme tedy používat taktiku [intro].*)

  (** Z [False] lze odvodit cokoli (to je pravidlo sporu "ex falso quodlibet").
  Můžeme využít taktiku [exfalso].*)
  Lemma not_contrad :  ~(P /\ ~P).
  Proof.
    intro H.
    destruct H as [p notp].
    apply notp.
    apply p.
  Qed.

  Lemma or_and_not : (P \/ Q) /\ ~P -> Q.
  Proof.
    intros [H notp].
    destruct H.
    - exfalso.
      apply notp.
      apply H.
    - apply H.
  Qed.

  Lemma de_morgan_1 : ~(P \/ Q) -> ~P /\ ~Q.
  Proof.
    intro H.
    split; intro K.
    - apply H.
      left.
      assumption.
    - apply H.
      right.
      assumption.
  Qed.

  Lemma de_morgan_2 : ~P /\ ~Q -> ~(P \/ Q).
  Proof.
    intros [notp notq].
    intro H.
    destruct H as [p | q].
    - apply notp, p.
    - apply notq, q.
  Qed.

  Lemma de_morgan_3 : ~P \/ ~Q -> ~(P /\ Q).
  Proof.
    intros H [p q].
    destruct H as [n | n];
    apply n; assumption.
  Qed.

  Lemma not_not_exm : ~ ~ (P \/ ~ P).
  Proof.
    intro H.
    cut (~ P).
    - intro notp.
      apply H.
      right.
      apply notp.
    - intro p.
      apply H.
      left.
      apply p.
  Qed.

End negace_kontradikce.

Section min_logika_programy.

  Hypotheses P Q R S : Prop.

  Lemma test (p : P) (f : P -> Q) : Q.
  Proof (f p).

  Lemma test' (p : P) : (P -> Q) -> Q.
  Proof
    fun f : P -> Q => f p.

  Lemma test'' : P -> (P -> Q) -> Q.
  Proof
    fun p : P =>
      fun f : P -> Q => f p.

  Print test''.

  Lemma test_tactics : P -> (P -> Q) -> Q.
  Proof.
    intros p f.
    apply f.
    apply p.
  Qed.

  Print test_tactics.

  Lemma id_P : P -> P.
  Proof
    fun p => p.

  Lemma id_PP : (P -> P) -> P -> P.
  Proof
    fun f => f.

  Lemma id_PP' : (P -> P) -> P -> P.
  Proof
    fun f p => p.

  Lemma ignore_Q : (P -> R) -> P -> Q -> R.
  Proof
    fun f p q => f p.

  Lemma delta_impR : (P -> Q) -> P -> P -> Q.
  Proof
    fun f p p' => f p.

  Lemma imp_trans : (P -> Q) -> ((Q -> R) -> (P -> R)).
  Proof
    fun f g p => g (f p).

  Lemma delta_imp : (P -> P -> Q) -> P -> Q.
  Proof
    fun f p => f p p.

  Lemma imp_dist : (P -> Q -> R) -> (P -> Q) -> P -> R.
  Proof
    fun f g p => f p (g p).

  Lemma imp_perm : (P -> Q -> R) -> Q -> P -> R.
  Proof
    fun f q p => f p q.

  Lemma diamond : (P -> Q) -> (P -> R) -> (Q -> R -> S) -> P -> S.
  Proof
    fun f g h p => h (f p) (g p).

  Lemma weak_peirce : ((((P -> Q) -> P) -> P) -> Q) -> Q.
  Proof (fun H =>
              H (fun K =>
                  K (fun p =>
                      H (fun L => p)))).

End min_logika_programy.

