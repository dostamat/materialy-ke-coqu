Section test.
Context (n m : nat) (E : n = m) (P : nat -> Prop) (H : P n).

Definition pm : P m.
Proof.
  refine
  (
  match E with
  | eq_refl => H
  end
  ).
Defined.

Print pm.

End test.