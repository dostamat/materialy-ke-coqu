(*
Definovat funkci
pred : nat -> nat
Problem: 0
1. reseni: nat -> (option nat)
pred : nat -> bud vystup previous,
       nebo vystup dukaz, ze vstup je 0.
*)

Check sumor.
Print sumor.

Print sig.

Definition pred_well_specified (n : nat) :
  { p : nat | n = S p } + {n = 0} :=
  match n with
  | O => inright _ (eq_refl 0)
  | S q => inleft _ (exist _ q (eq_refl (S q)))
  end.

Definition pred_well_specd_tactictd (n : nat) :
  { p : nat | n = S p } + {n = 0}.
Proof.
  destruct n as [| q].
  - right. reflexivity.
  - left. apply (exist _ q). reflexivity.
Defined.

Print pred_well_specd_tactictd.

Definition spec_pred'' (n : nat) : {p : nat | n = S p} + {n = 0}.
Proof.
  refine (
    match n with
    | O => inright _
    | S q => inleft _ (exist _ q _)
    end
  ); reflexivity.
Defined.

Require Extraction.

Extraction Language OCaml.
(* Extract Inductive sumor => "option" ["Some" "None"].*)
Extraction pred_well_specified.

Extraction Language Haskell.
Extraction pred_well_specified.

Inductive btree : Set :=
  | empty
  | node (n : nat) (t1 t2 : btree).

Inductive occurs (n : nat) : btree -> Prop :=
  | inroot (t1 t2 : btree) : occurs n (node n t1 t2)
  | inleft (m : nat) (t1 t2 : btree) (H : occurs n t1) :
      occurs n (node m t1 t2)
  | inright (m : nat) (t1 t2 : btree) (H : occurs n t2) :
    occurs n (node m t1 t2).

Example five_in_some_tree :
  occurs 5 (node 3 (node 5 empty empty) empty).
Proof.
  apply inleft.
  apply inroot.
Qed.

Print five_in_some_tree.

Check occurs 4 empty.

Require Import Arith.

Check Nat.eq_dec.

Print sumbool.

Definition naive_occurs_dec (n : nat) (t : btree) :
  {occurs n t} + {~ occurs n t}.
Proof.
  induction t.
  - right; intro H; inversion H.
  - pose (Nat.eq_dec n n0) as K.
    destruct K as [K1 | K2].
    + subst; left; constructor.
    + destruct IHt1, IHt2;
      try (left; constructor; assumption).
      * right. intro. inversion H; auto.
Defined.

Print naive_occurs_dec.

Extraction Language OCaml.
Extraction naive_occurs_dec.

Inductive lower (n : nat) (t : btree) : Prop :=
  | lower_intro (H : forall m, (occurs m t) -> n < m) :
      lower n t.

Inductive upper (n : nat) (t : btree) : Prop :=
  | upper_intro (H: forall m, (occurs m t) -> m < n) :
      upper n t.

Inductive search_tree : btree -> Prop :=
  | empty_search : search_tree empty
  | node_search
      (n : nat)
      (t1 t2 : btree)
      (ST1 : search_tree t1)
      (ST2 : search_tree t2)
      (U : upper n t1)
      (L : lower n t2)
      : search_tree (node n t1 t2).

Section props_of_search_trees.

Context
  (n : nat)
  (t1 t2 : btree)
  (se : search_tree (node n t1 t2)).

  Lemma search_tree_left : search_tree t1.
  Proof.
    inversion se; assumption.
  Qed.

  Lemma search_tree_right : search_tree t2.
  Proof.
    inversion se; assumption.
  Qed.

  Lemma upper_left : upper n t1.
  Proof.
    inversion se; assumption.
  Qed.

  Lemma lower_right : lower n t2.
  Proof.
    inversion se; assumption.
  Qed.

End props_of_search_trees.

Lemma lower_not_occurs (n : nat) (t : btree) :
  lower n t -> ~ occurs n t.
Proof.
  intros H O.
  destruct H.
  apply H in O.
  apply (Nat.lt_irrefl n).
  assumption.
Qed.

Lemma upper_not_occurs (n : nat) (t : btree) :
  upper n t -> ~ occurs n t.
Proof.
  intros H O.
  destruct H.
  apply H in O.
  apply (Nat.lt_irrefl n).
  assumption.
Qed.



Hint Resolve search_tree_left : st.
Hint Resolve search_tree_right : st.
Hint Resolve lower_not_occurs : st.
Hint Resolve upper_left : st.
Hint Resolve lower_right : st.

Definition occurs_dec
  (n : nat) (t : btree) (ST : search_tree t) :
    {occurs n t} + {~ occurs n t}.
Proof.
  induction t as [| m t1 IHt1 t2 IHt2].
  - right; intro H; inversion H.
  - destruct (Nat.eq_dec n m) as [H | H].
    + left; subst; constructor.
    + pose proof (search_tree_left _ _ _ ST) as STt1.
      pose proof (search_tree_right _ _ _ ST) as STt2.
      Search ( { _ < _ } + {~ _ < _ } ).
      destruct (lt_dec n m) as [nltm | not_nltm].
      * destruct (IHt1 STt1).
        -- left. constructor. assumption.
        -- right. intro K.
           inversion K; auto with st.
           assert (lower m t2).
           ++ inversion ST; assumption.
           ++ pose (H4 n H1) as L.
              pose proof (Nat.lt_trans _ _ _ nltm L) as nltn.
              Search ( (_ < _) -> ( _ < _ ) -> ( _ < _ ) ).
              Search (~ (_ < _) ).
              apply (Nat.lt_irrefl _ nltn).
      (*zde konec*)
      * assert (m < n).
        -- pose proof (nat_total_order n m) H as [M | M].
          exfalso. auto.
          assumption.
        -- destruct (IHt2 STt2).
        ++ left; constructor; assumption.
        ++ right. intro K.
          inversion K; auto.
          inversion ST.
          destruct U.
          (*pose proof (H5 n) as A.
          pose proof (A H2) as B.
          contradiction.*)
          auto.
Defined.

Print occurs_dec.

Extraction occurs_dec.

Inductive INSERT (n : nat) (t t' : btree) : Prop :=
  (* | from_empty (H : t = empty) (K : t' = node n empty empty) :
    INSERT n t t' *)
  | insert_intro
      (S : forall (m : nat), occurs m t -> occurs m t')
      (O : occurs n t')
      (S' : forall (m : nat), occurs m t'
              -> occurs m t \/ n = m)
      (ST : search_tree t') : INSERT n t t'.

Definition insert
  (n : nat)
  (t : btree)
  (ST : search_tree t) :
    {t' : btree | INSERT n t t' }.
Admitted.

Inductive REMOVE (n : nat) (t t' : btree) : Prop :=
  | remove_intro
      (H : ~ occurs n t')
      (S : forall (m : nat), occurs m t' -> occurs m t)
      (S' : forall (m : nat), occurs m t
              -> occurs m t'\/ n = m )
      (ST : search_tree t') : REMOVE n t t'.

Definition remove
  (n : nat)
  (t : btree)
  (ST : search_tree t) :
  { t' : btree | REMOVE n t t' }.
Admitted.

Lemma insert_leaf (n : nat) : INSERT n empty (node n empty empty).
Proof.
  constructor;
  auto 8.
  - intros. right.
    inversion H; auto;
    inversion H1.
  - constructor; auto.
    + constructor; intros; auto.
    inversion H.
    + constructor; intros; auto.
    inversion H.
Qed.

Lemma insert_l
  (n m : nat)
  (t1 t1' t2 : btree)
  (H: n < m)
  (ST : search_tree (node m t1 t2))
  (I : INSERT n t1 t1') :
  INSERT n (node m t1 t2) (node m t1' t2).
Proof.
  apply insert_intro.
  - intros p K.
