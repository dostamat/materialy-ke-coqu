Set Warnings "-notation-overridden".

(* Opakovani z minula: polymorfni list a neprijemne
opisovani typoveho argumentu *)
Inductive list (X : Type) : Type :=
  | nil
  | cons (x : X) (l : list X).

Check (nil nat) : list nat.
Check (cons nat 3 (nil nat)) : list nat.

(** Nastavme parametr [X] jako implicitni. *)
Arguments nil {X}.
Arguments cons {X}.

Check (cons 3 nil).
Check (cons 3 (cons 4 nil)).
Check nil. (* Nevime, jakeho typu je [nil]! *)

(** Chceme-li explicitne zminit implicitni argument,
pouzivame [@].*)
Check (@nil bool).

(** Polymorfni pary *)

Inductive prod (X Y : Type) : Type :=
| pair (x : X) (y : Y).

(** Typove argumenty nastavime jako implicitni. *)
Arguments pair {X} {Y}.

(** Zavedeme notaci pro pary. *)
Notation "( x , y )" := (pair x y).
Notation "X * Y" := (prod X Y) : type_scope.
(** Znovu zavedeme notaci pro listy, tentokrat polymorfni. *)
Notation "x :: y" := (cons x y)
                     (at level 60, right associativity).
Notation "[ ]" := nil.
Notation "[ x ; .. ; y ]" := (cons x .. (cons y []) ..).
Notation "x ++ y" := (app x y)
                     (at level 60, right associativity).

(** Znovu implementujte zakladni funkce pro praci s pary. *)
(** Vsimnete si slozenych zavorek kolem [X Y : Type].
Timto zpusobem nastavujeme tyto parametry jako implicitni. *)
Definition fst {X Y : Type} (p : X * Y) : X :=
  match p with
  | (x, y) => x
  end.

Compute (fst (true, 3)).


Definition snd {X Y : Type} (p : X * Y) : Y :=
  match p with
  | (x, y) => y
  end.

Compute (snd (true, 3)).

Fixpoint combine {X Y : Type} (lx : list X) (ly : list Y)
           : list (X*Y) :=
  match lx, ly with
  | nil, _ => nil
  | _, nil => nil
  | (x :: tx), (y :: ty) => (x, y) :: (combine tx ty)
  end.

Compute (combine [1;2;3;4] [true;false;true]).

(** Implementujte funkci rozdelujici list paru na par listu. *)
Fixpoint split {X Y : Type} (l : list (X*Y)) : (list X) * (list Y) :=
  match l with
  | nil => (nil, nil)
  | (x, y) :: t => let (tx, ty) := (split t) in
                   (x :: tx, y :: ty)
  end.

Example test_split:
  split [(1,false);(2,false)] = ([1;2],[false;false]).
Proof.
  simpl.
  reflexivity.
Qed.

(** Funkce vyssiho radu *)
Definition doit3times {X : Type} (f : X->X) (n : X) : X :=
  f (f (f n)).

Compute doit3times (fun x => 4 * x) 5.

(** Naprogramujte funkci [filter], ktera z listu [l] vybere
prvky splnujici vlastnost [test]. *)
Fixpoint filter {X:Type} (test: X->bool) (l:list X) : list X :=
  match l with
  | nil => nil
  | h :: t => if (test h) then (h :: filter test t)
                          else filter test t
  end.

Fixpoint even (n : nat) : bool :=
  match n with
  | O => true
  | S O => false
  | S (S n') => even n'
  end.

Compute filter even [1;2;3;4].

(** Naprogramujte funkci [num_of_evens],
ktera spocita pocet sudych cisel v listu. *)
Fixpoint length {X : Type} (l : list X) : nat :=
  match l with
  | nil => O
  | _ :: t => S (length t)
  end.

Definition num_of_evens (l : list nat) : nat :=
  length (filter even l).

Compute num_of_evens [1;2;3;4;5].

(** Naprogramujte funkci [partition], ktera rozdeli
seznam [l] na dva seznamy, z nich jeden obsahuje prvky z [l]
s vlastnost [test], a druhy obsahuje prvky z [l]
bez vlastnosti [test]. *)
Definition partition {X : Type}
                     (test : X -> bool)
                     (l : list X)
                   : list X * list X :=
  (filter test l , filter (fun x => negb (test x)) l).

Definition odd (n : nat) := negb (even n).

Compute partition odd [1;2;3;4;5].

Compute partition (fun x => false) [5;9;0].

(** Naprogramujte funkci [map], ktera na kazdy prvek listu [l]
aplikuje funkci [f]. *)
Fixpoint map {X Y : Type} (f : X->Y) (l : list X) : list Y :=
  match l with
  | nil => nil
  | h :: t => (f h) :: map f t
  end.

Compute map (fun x => plus 3) [2;0;2].

Compute map odd [2;1;2;5].

(** Co vrati nasledujici vypocet? Odpovezte pred vyhodnocenim. *)
Compute map (fun n => [even n;odd n]) [2;1;2;5].


(** NEPOVINNE CVICENI
---------------------
Dokazte, ze otoceni [reverse] a [map] spolu komutuji. *)
(* Zopakujeme definice [append] a [reverse] v polymorfni variante. *)
Fixpoint append {X : Type} (l1 l2 : list X) : list X :=
  match l1 with
  | nil      => l2
  | cons h t => cons h (append t l2)
  end.

Fixpoint reverse {X:Type} (l:list X) : list X :=
  match l with
  | nil      => nil
  | cons h t => append (reverse t) (cons h nil)
  end.

(** Pokud se v dukazu zaseknete, zkuste nejprve dokazat
vhodne pomocne tvrzeni. *)
Theorem map_append : forall (X Y : Type) (f : X -> Y) (l1 l2 : list X), map f (append l1 l2) = (append (map f l1) (map f l2)).
Proof.
  intros X Y f l1.
  induction l1 as [ | h t IH].
  - reflexivity.
  - intro l2.
    simpl.
    rewrite IH.
    reflexivity.
Qed.

Theorem map_reverse : forall (X Y : Type) (f : X -> Y) (l : list X),
  map f (reverse l) = reverse (map f l).
Proof.
  intros X Y f l.
  induction l as [ | h t IH].
  - reflexivity.
  - simpl.
    rewrite <- IH.
    apply map_append.
Qed.
(*
----------------------
*)

(** Currying a uncurrying *)

Definition prod_curry {X Y Z : Type}
  (f : X * Y -> Z) (x : X) (y : Y) : Z :=
    f (x, y).


(** Definujte [prod_uncurry], ktera
z funkce vyssiho radu vytvori funkci "dvou promennych",
tj. funkci, jejiz vstupnim typem bude [pair X Y].
*)
Definition prod_uncurry {X Y Z : Type}
  (f : X -> Y -> Z) (p : X * Y) : Z := 
  match p with
  | (x, y) => f x y
  end.

(** Jakeho typu je funkce [prod_curry] a [prod_uncurry]?
Zkuste odpovedet pred evaluaci. *)
Check @prod_curry.
Check @prod_uncurry.

(** Dokazte tvrzeni, ktera popisuji vztah mezi
"curried" a "uncurried" variantami funkci. *)
Theorem uncurry_curry : forall (X Y Z : Type)
                        (f : X -> Y -> Z)
                        x y,
  prod_curry (prod_uncurry f) x y = f x y.
Proof.
  intros X Y Z f x y.
  cbn.
  reflexivity.
Qed.

Theorem curry_uncurry : forall (X Y Z : Type)
                        (f : (X * Y) -> Z) (p : X * Y),
  prod_uncurry (prod_curry f) p = f p.
Proof.
 intros X Y Z f p.
 destruct p.
 simpl.
 reflexivity.
Qed.

