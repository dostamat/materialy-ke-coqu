Section unarni_predikaty.

  Context (U : Set) (P Q R : U -> Prop).
  
  (** Priklady s obecnymi kvantifikatory. *)

  (** K praci s obecnym kvantifikatorem pouzivame taktiky
[intro] (popripade [intros]) a [apply]. *)
  
  Lemma antecedent_out (a : U) :
    (forall x, P a -> Q x) -> (P a -> forall z, Q z).
  Proof.
    intros H Pa z.
    apply H.
    apply Pa.
  Qed.
  
  Lemma factor_forall_out :
    (forall x, P x) /\ (forall y, Q y) -> (forall z, P z /\ Q z).
  Proof.
    intros [allP allQ] z.
    split.
    - apply allP.
    - apply allQ.
   Qed.
   
  Lemma allPQ_allP_allQ : 
    (forall x, P x -> Q x) -> (forall y, P y) -> forall z, Q z.
  Proof.
    intros allPQ allP z.
    apply allPQ, allP.
  Qed.
   
  Lemma P_or_Q_but_not_P
   (H : forall x, P x \/ Q x)
   (K : forall y, ~ P y) :
   forall x, Q x.
  Proof.
    intros x.
    destruct (H x) as [Px | Qx].
    - exfalso; apply (K x), Px.
    - apply Qx.
  Qed.
  
  Lemma PQ_QR_PR
    (H : forall x, P x -> Q x)
    (K : forall x, Q x -> R x) :
    forall x, P x -> R x.
  Proof.
    intros x Px.
    apply K, H, Px.
  Qed.

  (** Priklady s existencnimi kvantifikatory. *)
  
  (** K praci s existencnim kvantifikatorem pouzivame taktiky
[exists] a [destruct]. *)
  
  Lemma exists_consequent (a : U) (H : exists x, P a -> Q x) :
    P a -> exists y, Q y.
  Proof.
    intro Pa.
    destruct H as [b Hb].
    exists b.
    apply Hb, Pa.
  Qed.
  
  Lemma factor_exists_in_conj :
    (exists x, P x /\ Q x) -> (exists y, P y) /\ (exists z, Q z).
  Proof.
    intros [a [Pa Qa]].
    split; exists a; assumption.
  Qed.
  
  Lemma factor_exists_in_disj :
    (exists x, P x \/ Q x) -> (exists y, P y) \/ (exists z, Q z).
  Proof.
    intros [x [Px | Qx]].
    - left. exists x. apply Px.
    - right. exists x. apply Qx.
  Qed.
  
End unarni_predikaty.

Section binarni_predikaty.

  Context (U : Set) (R : U -> U -> Prop).
  
  Lemma all_diagonal :
    (forall x, forall y, R x y) -> forall z, R z z.
  Proof.
    intros H z.
    apply H.
  Qed.

  Lemma technical (H : forall x y, R x y) :
    forall z, (R z z /\ forall t, R t z).
  Proof.
    intros z.
    split.
    - apply H.
    - intros t.
      apply H.
  Qed.
  
  Lemma ex_x_y_Rxy_Ryx (u : U) :
    exists x y, R x y -> R y x.
  Proof.
    assert (R u u -> R u u).
    - intro; assumption.
    - exists u, u.
      assumption.
  Qed.
  
End binarni_predikaty.


Section smisene_ulohy.

  Context (U : Set) (P Q S : U -> Prop).

  Lemma mix1 (H : forall x, P x -> Q x) (K : exists y, P y) :
    exists z, Q z.
  Proof.
    destruct K as [a Pa].
    exists a.
    apply (H a Pa).
  Qed.

  Lemma mix2 (H : forall x, Q x -> S x) (K : exists y, P y /\ Q y) :
    exists z, P z /\ S z.
  Proof.
    destruct K as [a [Pa Qa]].
    exists a.
    split.
    - assumption.
    - apply (H a Qa).
  Qed.
  
  Lemma mix3 (H : exists x, P x) (K : forall y z, P y -> Q z) :
    forall t, Q t.
  Proof.
    intro t.
    destruct H as [a Pa].
    apply (K a t Pa).
  Qed.

End smisene_ulohy.


Section slovni_uloha.

  (*
Every young and healthy person likes baseball.

Every active person is healthy.

Someone is young and active.

Therefore, someone likes baseball.
*)
  Context (U : Set) (A B Y H : U -> Prop).

  Lemma someone_likes_baseball
    (K : forall x, Y x /\ H x -> B x)
    (L : forall y, A y -> H y)
    (M : exists z, Y z /\ A z) :
    exists t, B t.
  Proof.
    destruct M as [u [Yu Au]].
    exists u.
    apply K.
    split.
    - apply Yu.
    - apply L.
      apply Au.
  Qed.

End slovni_uloha.