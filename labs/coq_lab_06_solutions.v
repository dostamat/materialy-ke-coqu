(** Vyrazne vybrano z kapitol [Lists] a [Poly]
knihy Logical Foundations! (todo: add link) *)

Set Warnings "-notation-overridden".

Module playground.

Inductive natprod : Type :=
  | pair (n1 n2 : nat).

Check pair 5 4.

Definition fst (p : natprod) :=
  match p with
  | (pair n1 n2) => n1
  end.

Check fst.

Compute fst (pair 2 4).

Definition snd (p : natprod) :=
  match p with
  | (pair n1 n2) => n2
  end.

Notation "( x , y )" := (pair x y).

Check (2, 3).

Compute snd (2, 3).

Definition swap (p : natprod) : natprod :=
  match p with
  | (pair x y) => (pair y x)
  end.

Definition swap' (p : natprod) : natprod :=
  match p with
  | (x , y) => (y, x)
  end.

Print swap.

Lemma pairing_lemma : forall (n m : nat),
  (n, m) = (fst (n, m), snd (n, m)).
Proof.
  intros n m.
  simpl.
  reflexivity.
Qed.

Lemma pair_constructed_from_destructed: forall (p: natprod),
  p = (fst p, snd p).
Proof.
  intro p.
  destruct p as [n m].
  simpl.
  reflexivity.
Qed.

Lemma snd_fst_swap : forall (p : natprod),
  (snd p, fst p) = (swap p).
Proof.
  intro p.
  destruct p as [n m].
  reflexivity.
Qed.

(** List prirozenych cisel *)

Inductive natlist :=
  | nil
  | cons (n : nat) (l : natlist).

Definition mujlist := (cons 3 (cons 2 nil)).

Notation "x :: l" := (cons x l)
                     (at level 60, right associativity).

Print mujlist.

Fixpoint repeat (n : nat) (count : nat) : natlist :=
  match count with
  | O => nil
  | S count' => n :: (repeat n count')
  end.

Compute (repeat 13 7).

Fixpoint length (l : natlist) : nat :=
  match l with
  | nil => O
  | _ :: tail => S (length tail)
  end.

Compute (length (3 :: 2 :: 4 :: 7 :: nil)).

Fixpoint append (l1 l2 : natlist) : natlist :=
  match l1 with
  | nil => l2
  | head :: tail => head :: (append tail l2)
  end.

Compute append (1 :: 2 :: nil) (3 :: 4 :: nil).

Notation "x ++ y" := (append x y)
                     (right associativity, at level 60).

Compute (1 :: 2 :: nil) ++ (3 :: 4 :: nil).

(**
--------------------------------------------------
Cviceni pro narocnejsi: implementujte nasledujici.

(* Vybere z listu nenulove prvky. *)
*)
Fixpoint nonzeros (l : natlist) : natlist :=
  match l with
  | nil => nil
  | O :: tail => (nonzeros tail)
  | n :: tail => n :: (nonzeros tail)
  end.

Compute (nonzeros (1 :: 0 :: 2 :: 0 :: nil)).

(* Alternuje prvky prvniho a druheho listu. *)
Fixpoint alternate (l1 l2 : natlist) : natlist :=
  match l1 with
  | nil => l2
  | (head :: tail) => match l2 with
                      | nil => (head :: tail)
                      | (head2 :: tail2) =>
                        head :: head2 ::
                          (alternate tail tail2) 
                      end
  end.

Compute (alternate (1 :: 3 :: nil) (2 :: 4 :: nil)).
(*
--------------------------------------------------
*)


(** Spojovani listu je asociativni: *)
Theorem app_assoc : forall l1 l2 l3 : natlist,
  (l1 ++ l2) ++ l3 = l1 ++ (l2 ++ l3).
Proof.
  intro l1.
  induction l1 as [ | head tail IH].
  (* [l1] je [nil] *)
  - reflexivity.
  (* [l1] je [head :: tail] *)
  - intros l2 l3.
    simpl.
    rewrite IH.
    reflexivity.
Qed.

(** Implementujte otoceni listu.
(Nemusite resit casovou efektivitu sve implementace.) *)
Fixpoint reverse (l : natlist) : natlist :=
  match l with
  | nil => nil
  | head :: tail => (reverse tail) ++ (head :: nil)
  end.

Compute (reverse (1 :: 2 :: 3 :: nil)).

(** Delka spojeni dvou seznamu je souctem delek spojovanych seznamu. *)
Theorem append_length : forall l1 l2 : natlist,
  length (l1 ++ l2) = (length l1) + (length l2).
Proof.
  intro l1.
  induction l1 as [ | head tail IH].
  (* [l1] je [nil] *)
  - simpl. reflexivity.
  (* [l1] je [head :: tail] *)
  - simpl.
    intro l2.
    rewrite IH.
    reflexivity.
Qed.


(** Delka otoceneho seznamu se rovna delce puvodniho seznamu. *)
Theorem reverse_length : forall l : natlist,
  length (reverse l) = length l.
Proof.
  intro l.
  induction l as [ | head tail IH].
  (* [l] je [nil] *)
  - simpl.
    reflexivity.
  (* [l] je [head :: tail] *)
  - simpl.
    rewrite append_length.
    simpl.
    rewrite IH.
    assert (forall n, n + 1 = S n).
    + induction n.
      reflexivity.
      simpl.
      rewrite IHn.
      reflexivity.
    + rewrite H. reflexivity.
Qed.


(** Vybirani [n]-teho prvku v listu muze skoncit katastrofou,
pokud je [n] vetsi nez pocet prvku v listu. *)

Inductive natoption : Type :=
  | Some (n : nat)
  | None.

(** Funkce vybere [n]-ty prvek jako [Some n], jinak vrati [None]. *)
Fixpoint nth (l:natlist) (n:nat) : natoption :=
  match l, n with
  | nil, _ => None
  | h :: _, O => Some h
  | _ :: t, S n' => nth t n'
  end.

Notation "[ x ; .. ; y ]" := (cons x .. (cons y nil) ..).

Example test_nth1 : nth [4;5;6;7] 0 = Some 4.
Proof. reflexivity. Qed.
Example test_nth2 : nth [4;5;6;7] 3 = Some 7.
Proof. reflexivity. Qed.
Example test_nth3 : nth [4;5;6;7] 9 = None.
Proof. reflexivity. Qed.

End playground.

(** Coq umoznuje definovat *polymorfni* datove typy.
Nemusime pro kazdy datovy typ psat novou definici
listu pro dany typ. *)

Inductive list (X : Type) : Type :=
  | nil
  | cons (x : X) (l : list X).

Check (nil nat).
Check (cons nat 3 (nil nat)).

(** Nechutne cviceni: *)

Module MumbleGrumble.

Inductive mumble : Type :=
  | a
  | b (x : mumble) (y : nat)
  | c.

Inductive grumble (X:Type) : Type :=
  | d (m : mumble)
  | e (x : X).

Check (b a 5).

Check d mumble (b a 5).

Check d bool (b a 5).

Check e bool true.

Check (b c 0).

Fail Check e bool (b c 0).

Check c.

(** Ktere z nasledujicich termu jsou elementy [grumble X]
pro nejaky typ X?
      - [d (b a 5)] NE
      - [d mumble (b a 5)] ANO, pro X = mumble.
      - [d bool (b a 5)] ANO, pro X = bool.
      - [e bool true] ANO, pro X = bool.
      - [e mumble (b c 0)] ANO, pro X = mumble.
      - [e bool (b c 0)] NE.
      - [c] NE, [c] je typu mumble. *)
      
End MumbleGrumble.

(** Polymorfni implementace funkce [repeat]
s typovymi anotacemi: *)
Fixpoint repeat (X : Type) (x : X) (count : nat) : list X :=
  match count with
  | 0 => nil X
  | S count' => cons X x (repeat X x count')
  end.

(** Co se stane, kdyz nebudeme typy anotovat? *)
Fixpoint repeat' X x count : list X :=
  match count with
  | 0        => nil X
  | S count' => cons X x (repeat' X x count')
  end.

Check repeat.
Check repeat'.
(** Coq se snazi typy sam odvozovat (provadi *typovou inferenci* ). *)

(** Pouzitim zoliku [_] muzeme dale Coq nutit odvozovat typy: *)
Fixpoint repeat'' X x count : list X :=
  match count with
  | 0        => nil _
  | S count' => cons _ x (repeat'' _ x count')
  end.

(** Tim si muzeme usnadnit praci: *)
Definition list123 :=
  cons nat 1 (cons nat 2 (cons nat 3 (nil nat))).

Definition list123' :=
  cons _ 1 (cons _ 2 (cons _ 3 (nil _))).

(** To je stale otravne. At Coq implicitne typy vyvozuje. *)
Arguments nil {X}.
Arguments cons {X}.
Arguments repeat {X}.

Definition list123'' := cons 1 (cons 2 (cons 3 nil)).

(** Notace pro listy, tentokrat polymorfni. *)
Notation "x :: y" := (cons x y)
                     (at level 60, right associativity).
Notation "[ ]" := nil.
Notation "[ x ; .. ; y ]" := (cons x .. (cons y []) ..).
Notation "x ++ y" := (app x y)
                     (at level 60, right associativity).

(** Polymorfni pary *)

Inductive prod (X Y : Type) : Type :=
| pair (x : X) (y : Y).

(** Typove argumenty nastavime jako implicitni. *)
Arguments pair {X} {Y}.

(** Zavedeme notaci pro pary. *)
Notation "( x , y )" := (pair x y).

Notation "X * Y" := (prod X Y) : type_scope.

(** Znovu implementujte zakladni funkce pro praci s pary. *)
Definition fst {X Y : Type} (p : X * Y) : X.
Admitted.

Definition snd {X Y : Type} (p : X * Y) : Y.
Admitted.

Fixpoint combine {X Y : Type} (lx : list X) (ly : list Y)
           : list (X*Y).
Admitted.

(** Implementujte funkci rozdelujici list paru na par listu. *)
Fixpoint split {X Y : Type} (l : list (X*Y)) : (list X) * (list Y).
Admitted.

Example test_split:
  split [(1,false);(2,false)] = ([1;2],[false;false]).
Proof.
  reflexivity.
Qed.