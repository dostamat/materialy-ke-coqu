(** Induktivni definice sudych prirozenych cisel: *)
Inductive ev : nat -> Prop :=
  | ev_0 : ev 0
  | ev_SS : forall (n : nat), ev n -> ev (S (S n)).

(**Dokazte, ze 4 je suda: *)
Lemma ev_4 : ev 4.
Proof.
  repeat constructor.
Qed.

Print ev_4.

Fixpoint double (n : nat) : nat :=
  match n with
  | O => O
  | S n' => S (S (double n'))
  end.

(**Dokazte, ze dvojnasobek kazdeho cisla
je cislo sude.*)
Lemma ev_double : forall (n : nat), ev (double n).
Proof.
  intro n.
  induction n.
  - simpl.
    apply ev_0.
  - simpl.
    apply ev_SS.
    assumption.
Qed.


(** Dokazte "inverzni" lemma: *)
Lemma ev_inversion :
  forall (n : nat), ev n ->
  (n = 0) \/ (exists n', n = S (S n') /\ ev n').
Proof.
  intros n H.
  destruct H as [ | m K].
  - left.
    reflexivity.
  - right.
    exists m.
    split.
    + reflexivity.
    + assumption.
Qed.

(* Dokazte (staci [destruct]):*)
Lemma ev_minus2 :
  forall (n : nat), ev n -> ev (pred (pred n)).
Proof.
  intros n H.
  destruct H as [ | m K].
  - simpl.
    constructor.
  - simpl.
    assumption.
Qed.

(* Dokazte, na predpoklad uzijte [ev_inversion]
([apply ev_inversion in H]).
*)
Lemma evSS_ev :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
  intros n H.
  apply ev_inversion in H.
  destruct H.
  - discriminate.
  - destruct H as [w K].
    destruct K as [K1 K2].
    injection K1 as E.
    rewrite E.
    assumption.
Qed.

(** Jednodussi zpusob je pouzit taktiku [inversion].
  Ta automatizuje praci, kterou jsme delali manualne. *)
Lemma evSS_ev' :
  forall (n : nat), ev (S (S (S n))) -> ev (S n).
Proof.
  intros n H.
  inversion H.
  - assumption.
Qed.

(** Dokazte, ze 1 neni suda.
([inversion] pomaha.) *)
Lemma one_not_ev : ~ ev 3.
Proof.
  unfold not.
  intro H.
  inversion H.
  - inversion H1.
Qed.


(** Dokazte s vyuzitim [inversion] :*)
Theorem SSSSev__even : forall n,
  ev (S (S (S (S n)))) -> ev n.
Proof.
  intros n H.
  inversion H.
  clear H0 n0.
  inversion H1.
  assumption.
Qed.

(** Dokazte s vyuzitim [inversion] :*)
Theorem ev5_nonsense :
  ev 5 -> 2 + 2 = 9.
Proof.
  intros H.
  inversion H.
  inversion H1.
  inversion H3.
Qed.

(** Alternativni, "neinduktivni" definice sudosti: *)
Definition Even x := exists n : nat, x = double n.

(** Obcas je treba pouzit indukci podle predikatu: *)
(** (nezapomenme na [unfold]!) *)
Lemma ev_Even : forall n,
  ev n -> Even n.
Proof.
  intros n H.
  induction H.
  - unfold Even.
    exists 0.
    reflexivity.
  - unfold Even in *.
    destruct IHev.
    rewrite H0.
    exists (S x).
    reflexivity.
Qed.

(** Dokazte indukci podle predikatu: *)
Lemma ev_sum : forall (n m : nat), ev n -> ev m -> ev (n + m).
Proof.
  intros.
  induction H.
  - trivial.
  - simpl.
    constructor.
    apply IHev.
Qed.

(** Dokazte obraceny smer ekvivalence nasich dvou
definici sudosti. *)
Lemma Even_ev : forall n, Even n -> ev n.
Proof.
  intros n H.
  unfold Even in H.
  destruct H as [w K].
  rewrite K.
  apply ev_double.
Qed.

(** Zde je dalsi mozna definice sudosti: *)
Inductive ev' : nat -> Prop :=
  | ev'_0 : ev' 0
  | ev'_2 : ev' 2
  | ev'_sum : forall (n m : nat),
              ev' n -> ev' m -> ev' (n + m).

(** Dokazte ekvivalenci techto dvou
induktivnich definic. *)
Lemma ev'_ev : forall (n : nat), ev' n <-> ev n.
Proof.
  split.
Admitted.

(** Dokazte indukci (ale podle ceho?): *)
Theorem ev_ev__ev : forall n m,
  ev (n+m) -> ev n -> ev m.
Proof.
Admitted.

(** Navrat k logice. *)

(** V Coqu jsou logicke spojky representovany
jako induktivni typy.
Vyjimky:
- obecna kvantifikace (dependent product)
- implikace (function type)
- negace (simulovano pomoci implikace a False) *)

(** Jak definovat [True]? *)
Inductive True : Prop :=
  | I : True.

(** [True] ma jediny konstruktor [I].
[I] je (jedinym) dukazem vyroku [True]. *)

Check I.

(** Jak definovat [False]? *)
Inductive False : Prop :=
  .

(** [False] nema zadny konstruktor!
Samozrejme: nechceme mit zpusob, jak
dokazat [False]. *)

Print and.

(** Jak definovat konjunkci [and]? *)
Inductive and (A B : Prop) : Prop :=
  conj : A -> B -> and A B.

(** [and] ma jediny konstruktor:
[conj] ocekava dukaz vyroku [A] a dukaz vyroku [B],
rekneme [a : A] a [b : B] a umoznuje
sestrojit dukaz vyroku [and A B],
ktery se nazyva [conj A B a b].*)

Section konjunkce_priklad.

Context (A B : Prop) (a : A) (b : B).

Check conj A B a b.

End konjunkce_priklad.


(** Jak definovat disjunkci [or]? *)
Inductive or (A B : Prop) : Prop :=
  | or_introl : A -> or A B
  | or_intror : B -> or A B.

(** [or] ma dva konstruktory:
[or_introl] ocekava dukaz vyroku [A]
a umoznuje sestrojit dukaz vyroku [or A B],
[or_intror] ocekava dukaz vyroku [B]
a umoznuje sestrojit dukaz vyroku [or A B].*)

Section disjunkce_priklad.

Context (A B C: Prop) (a : A) (b : B).

Check or_introl A C a.

Check or_intror A B b.

End disjunkce_priklad.

(** Jak definovat existencni kvantifikator? *)
Inductive ex (A : Type) (P : A -> Prop) : Prop :=
  | ex_intro (x : A) (p : P x) : ex A P.


(** Mame-li typ [A] a vlastnost [P : A -> Prop],
[ex A P] ma byt vyrok tvrdici
"existuje prvek z A s vlastnosti P".
Jak (konstruktivne) dokazat,
ze nejaky takovy prvek existuje?
Nejprve musime predlozit prvek [x : A],
a pote musime predlozit dukaz, ze
[x] ma vlastnost [P], tedy nejaky dukaz [p : P x].
To staci k sestrojeni dukazu tvrzeni [ex A P].

Presne to umoznuje konstruktor [ex_intro].
*)

Section existence_priklad.

Check ev_4.
Print ev_4.

Check ex_intro nat ev 4 ev_4.

(** Existuje sude cislo: *)
Definition exists_even_nat : ex nat ev :=
  ex_intro nat ev 4 ev_4.

End existence_priklad.

(** Dokonce i rovnost lze definovat induktivne: *)
Inductive equal (A : Type) : A -> A -> Prop :=
  equal_refl : forall x : A, equal A x x.

(** Poznamka: toto neni nejjednodussi definice
rovnosti; Coq interne pouziva mirne jinou.*)

Section logika_priklady.

Context (A B C P Q R S : Prop).

(** Vyreste stare zname ulohy.
Napiste primo dukazove termy, tj. programy
odpovidajici dukazum danych tvrzeni. *)

(** Pokud chcete, muzete ucinit
"proposicni" argumenty implicitnimi:
Arguments conj {A B}.
Arguments or_introl {A B}.
Arguments or_intror {A B}.
*)
Definition and_assoc : (and P (and Q R)) -> (and (and P Q) R) :=
  (fun (proof : (and P (and Q R)))
    => match proof with
       | conj _ _ p qr => match qr with
                          | conj _ _ q r =>
                          conj _ _ (conj _ _ p q) r
                          end
        end
    ).

Arguments conj {A B}.

Definition and_assoc' : (and P (and Q R)) -> (and (and P Q) R) :=
  (fun (proof : (and P (and Q R)))
    => match proof with
       | conj p qr => match qr with
                          | conj q r =>
                          conj (conj p q) r
                          end
        end
    ).

Arguments or_introl {A B}.
Arguments or_intror {A B}.

Definition and_imp_dist : (and (P -> Q) (R -> S)) ->
                        ((and P R) -> (and Q S)) :=
  fun pqrs =>
    match pqrs with
    | conj pq rs =>
      (fun p_and_r =>
        match p_and_r with
        | conj p r => conj (pq p) (rs r)
        end)
    end.


Definition uncurrying : (A -> (B -> C)) -> ((and A B) -> C) :=
  fun a_b_c =>
    fun a_and_b =>
      match a_and_b with
      | conj a b =>
          (a_b_c a b)
      end.

Definition currying : ((and A B) -> C) -> (A -> (B -> C)) :=
  fun a_and_b__c a b => a_and_b__c (conj a b).
(* fun a_and_b__c =>
    fun a =>
      fun b =>
        a_and_b__c (conj a b). *)

Definition or_comm : (or A B) -> (or B A) :=
  fun a_or_b =>
    match a_or_b with
    | or_introl a => or_intror a
    | or_intror b => or_introl b
    end.

Definition either_implies :
  ((or A B) -> C) -> (and (A -> C) (B -> C)) :=
  fun a_or_b__c =>
    conj (fun a => a_or_b__c (or_introl a))
         (fun b => a_or_b__c (or_intror b)).

End logika_priklady.

Check sig.
Print sig.

Check @sig nat (fun n => n < 5).

Locate "<".
Print lt.

Lemma lt_3_5 : 3 < 5.
Proof.
  repeat constructor.
Qed.

Definition three_in_subset :=
  exist (fun n => n < 5) 3 lt_3_5.

Check three_in_subset.

Definition extract (A : Set) (P : A -> Prop) :
  {x : A | P x} -> A :=
  fun x =>
  match x with
  | exist _ a Pa => a
  end.

Compute extract _ _ three_in_subset.

Check sumbool.
Print sumbool.

Definition eq_dec (A : Set) :=
  forall x y : A, {x = y} + {~ x = y}.

Definition nats_equal : nat -> nat -> bool :=
  fun n m => false.

Lemma nat_eq_dec : eq_dec nat.
Proof.
  unfold eq_dec.
  intros x.
  induction x.
  - destruct y.
    + apply left.
      reflexivity.
    + apply right.
      discriminate.
  - destruct y.
    + apply right.
      discriminate.
    + pose proof (IHx y) as IHxy.
      destruct IHxy.
      -- apply left.
         rewrite e.
         reflexivity.
      -- apply right.
         intro.
         injection H as K.
         unfold not in n.
         apply n.
         apply K.
Qed.