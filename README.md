# Materiály ke Coqu

## Jak rozjet Coq

Pokud nechcete nic instalovat, můžete používat **jsCoq** v prohlížeči: [jsCoq](https://coq.vercel.app/scratchpad.html)

Pokud si chcete Coq nainstalovat, můžete stáhnout [Coq platform](https://coq.inria.fr/download).

### GUI

Můžete používat CoqIDE, které se nainstaluje s Coqem.

Pro uživatele emacsu existuje plugin [Proof General](https://proofgeneral.github.io/).

Pro uživatele VSCode existuje plugin [VSCoq](https://github.com/coq-community/vscoq)